Antelope data

Source: Theodore Garland, tgarland@ucr.edu

Based on Jarman 1974, Brashares 2000. There are some differences in the species data between this version and version of Brashares.

Height: shoulder height in cm

Antipredator class: 0 = hide, 1 = flee