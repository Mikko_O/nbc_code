
# load libraries
library("ape")
library("phylolm")

# sample phylogeny

n <- 20
set.seed(54321)
t <- rcoal(n)

# sample y
y <- rbinTrait(1, phy = t, beta = c(0.5), alpha = 5)

# What does rbinTrait do? 1) binary Markov process on the tree. 2) 

# sample x
x <- vector("double", n)
names(x) <- t$tip.label
species0 <- names(y[y == 0])
species1 <- names(y[y == 1])
x[species0] <- rTrait(1, keep.tip(t, species0), model = "BM",
                      parameters = list(ancestral.state=-1))
x[species1] <- rTrait(1, keep.tip(t, species1), model = "BM",
                      parameters = list(ancestral.state=1))


# fit logistic regression
lr.model <- phyloglm(y ~ x, phy = t)

summary(lr.model)
# fit gaussian NB
pi1 <- 1/(1+exp(- phyloglm(y ~ 1, phy = t)$coefficients[1]))

mu0 <- mean(x[species0])
mu1 <- mean(x[species1])
sd0 <- sd(x[species0])
sd1 <- sd(x[species1])
nb.discriminant <- function(x) dnorm(x, mu1, sd1, log = TRUE) - dnorm(x, mu0, sd0, log = TRUE) +
  log(pi1/(1-pi1))

# compute accuracies
lr.decision.point <- -lr.model$coefficients[1] / lr.model$coefficients[2]
nb.decision.point <- 


# compare decision points and accuracies of the classifiers

# compare decision points with regards to training data size

