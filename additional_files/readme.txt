Readme

train_nb.R
functions to train phylogenetic and classical Naive Bayes classifiers.
dataThis folder contains data on extant antelopes, their dental traits, phylogeny, and fossil specimens.fossil_prediction.RUse trained model to predict anti-predator behavior of fossil antelopes.gaussian_naive_bayes.RmdFit Gaussian Naive Bayes on the antelope data.gaussian_nb_bootstrap.RmdFit model on extant data and compute uncertainty intervals with parametric bootstrap.normal_distribution_estimators.Rmd
This file contains empirical evaluation of normal distribution estimation procedures.lambda_experiments.RLambda model likelihood profiles for one model.phylosignal.RPhylogenetic signal in dental traits of extant African antelopes.plotsThis folder contains some plots.

cluster_computingCode and data in this folder was used in empirical evaluation of prior probability estimation.comparisons.RTrain some other models on the antelope data (logistic regression, discriminant function analysis).